package readexcel;



import java.io.FileInputStream;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebElement;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import java.util.List;

public class CreatePartnerandMember {

	public static void main(String[] args) throws BiffException, IOException, InterruptedException 
	{
		WebDriver driver = new FirefoxDriver();
		String FilePath = "C:\\ConsultantsBench\\CreatepartnerAndmember.xls";
		FileInputStream fs = new FileInputStream(FilePath);
		Workbook wb = Workbook.getWorkbook(fs);
		Sheet sh = wb.getSheet("partner&member");
		int totalNoOfRows = sh.getRows();
		int totalNoOfCols = sh.getColumns();
		for (int row = 0; row < totalNoOfRows; row++) 
		{
				for (int col = 0; col < totalNoOfCols; col++) 
				{
					System.out.print(sh.getCell(col, row).getContents() + "\t");
				}
				System.out.println();
		}
		        //navigate url
				driver.get("http://stage.consultantsbench.com/member/decideUrl");
				//Type in Emailid and password
				driver.findElement(By.id("username")).sendKeys(""+sh.getCell(0, 0).getContents());
				driver.findElement(By.id("password")).sendKeys(""+sh.getCell(1, 0).getContents());
				Thread.sleep(2000);
		        //click on sing in button
				driver.findElement(By.id("signInBtn")).click();
				//click on create partner
				WebDriverWait wait = new WebDriverWait(driver, 30); 
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='createPrtnrLink']/a"))).click();
				//driver.findElement(By.xpath("//*[@id='createPrtnrLink']/a")).click();
				Thread.sleep(2000);
				//Type in company name
				driver.findElement(By.id("companyName")).sendKeys(""+sh.getCell(0, 1).getContents());
				Thread.sleep(2000);
				//Select the industry
				Select select = new Select(driver.findElement(By.xpath("//*[@id='industry']")));
				select.selectByVisibleText("Fashion");
				Thread.sleep(2000);
				//Type in address
				driver.findElement(By.id("addressLine1")).sendKeys(""+sh.getCell(0, 2).getContents());
				Thread.sleep(2000);
				driver.findElement(By.id("addressLine2")).sendKeys(""+sh.getCell(0, 3).getContents());
				Thread.sleep(2000);
				//Select on country
				Select select1 = new Select(driver.findElement(By.xpath("//*[@id='country']")));
				select1.selectByVisibleText("United States");
				//Select on city
				Select select2 = new Select(driver.findElement(By.xpath("//*[@id='city']")));
				select2.selectByVisibleText("Bengaluru");
				Thread.sleep(2000);
				//Type in postal code
				driver.findElement(By.id("postalCode")).sendKeys(""+sh.getCell(0, 4).getContents());
				//Type in company description
				driver.findElement(By.id("companyDesc")).sendKeys(""+sh.getCell(0, 5).getContents());
				Thread.sleep(2000);
				// select exclude Partners Display
				Select select3 = new Select(driver.findElement(By.xpath("//*[@id='excludePartnersDisplay']")));
				select3.selectByVisibleText("Consultant Bench");
				Thread.sleep(2000);
				//select exclude Partners Publish
				Select select4 = new Select(driver.findElement(By.xpath("//*[@id='excludePartnersPublish']")));
				select4.selectByVisibleText("Enstrapp IT Solutions Pvt Ltd");
				Thread.sleep(2000);
				//select in multiple companyOnboardingDocs
				Select select5 = new Select(driver.findElement(By.xpath("//*[@id='companyOnboardingDocs']")));
				select5.selectByVisibleText("Photo Copy Passport");
				select5.selectByVisibleText("Pan/Ration Cards");
				select5.selectByVisibleText("Passport/Stamp size Photo");
				select5.selectByVisibleText("Last Employer Pay Slip");
				Thread.sleep(2000);
				//KEY CONTACT PERSON
		        //Type in contact person first name
				driver.findElement(By.id("kContactFirstName")).sendKeys(""+sh.getCell(0, 6).getContents());
				Thread.sleep(2000);
				//Type in contect person last name
				driver.findElement(By.id("keyContact.lastName")).sendKeys(""+sh.getCell(0, 7).getContents());
				Thread.sleep(2000);
		        //Type in location
				driver.findElement(By.id("keyContact.location")).sendKeys(""+sh.getCell(0, 8).getContents());
				Thread.sleep(2000);
				//Type in EMailID
				driver.findElement(By.id("kContactEmail")).sendKeys(""+sh.getCell(0, 9).getContents());
				Thread.sleep(2000);
				//Type in contact number
				driver.findElement(By.id("kContactPhone")).sendKeys(""+sh.getCell(0, 10).getContents());
				Thread.sleep(2000);
				//Type in google hangout id
				driver.findElement(By.id("keyContact.collaborationId")).sendKeys(""+sh.getCell(0, 11).getContents());
				//TAX INFORMATION
				//Type in service tax
				driver.findElement(By.id("servicesTax")).sendKeys(""+sh.getCell(0, 12).getContents());
				Thread.sleep(2000);
				//Type in VAT
				driver.findElement(By.id("valueAddedTax")).sendKeys(""+sh.getCell(0, 13).getContents());
				Thread.sleep(2000);
				//Type in PAN number
				driver.findElement(By.id("pan")).sendKeys(""+sh.getCell(0, 14).getContents());
				Thread.sleep(2000);
				//Type in TIN number
				driver.findElement(By.id("tin")).sendKeys(""+sh.getCell(0, 15).getContents());
				Thread.sleep(2000);
				//PAYMENT DETAILS
				//Type in BANKNAME
				driver.findElement(By.id("companyPaymentDetails.bankName")).sendKeys(""+sh.getCell(0, 16).getContents());
				Thread.sleep(2000);
				//Type in ACC No
				driver.findElement(By.id("companyPaymentDetails.accountNumber")).sendKeys(""+sh.getCell(0, 17).getContents());
				Thread.sleep(2000);
				//Type in ACC Holder name
				driver.findElement(By.id("companyPaymentDetails.accountHolderName")).sendKeys(""+sh.getCell(0, 18).getContents());
				Thread.sleep(2000);
				//Type in IFSC code
				driver.findElement(By.id("companyPaymentDetails.ifscCode")).sendKeys(""+sh.getCell(0, 19).getContents());
				Thread.sleep(2000);
				//Type in BRANCH
				driver.findElement(By.id("companyPaymentDetails.branch")).sendKeys(""+sh.getCell(0, 20).getContents());
				Thread.sleep(2000);
				//Click on create partner button
				driver.findElement(By.id("submitButton")).click();
				Thread.sleep(2000);
				//Click on partner list  
				driver.findElement(By.xpath("//*[@id='prtnrListLink']/a")).click();
				//Click on company enable checkbox
				List<WebElement> els = driver.findElements(By.xpath("//input[@type='checkbox']"));
		        for ( WebElement el : els ) 
		        {
		            if ( !el.isSelected() ) 
		            {
		                el.click();
			}

		}
		        
		        driver.navigate().back(); 
		        //click on create member
		        WebDriverWait wait5 = new WebDriverWait(driver, 30); 
				wait5.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='createMemberLink']/a"))).click();
		       // driver.findElement(By.xpath("//*[@id='createMemberLink']/a")).click();
				Thread.sleep(2000);
				//Type in username
				driver.findElement(By.id("username")).sendKeys(""+sh.getCell(0, 21).getContents());
				Thread.sleep(2000);
				//Type in password
				driver.findElement(By.id("password")).sendKeys(""+sh.getCell(0, 22).getContents());
				Thread.sleep(2000);
				//Select on company name
				Select select6=new Select(driver.findElement(By.id("company")));
				select6.selectByVisibleText("Accenture");
				//Type in first name
				driver.findElement(By.id("firstName")).sendKeys(""+sh.getCell(0, 23).getContents());
				Thread.sleep(2000);
				//Type in lastname
				driver.findElement(By.id("lastName")).sendKeys(""+sh.getCell(0, 24).getContents());
				Thread.sleep(2000);
				//Select on role
				Select select7=new Select(driver.findElement(By.id("role.id")));
				select7.selectByVisibleText("SUPER_USER");
				Thread.sleep(2000);
				//Type in contact number
				driver.findElement(By.id("contactNo")).sendKeys(""+sh.getCell(0, 25).getContents());
				Thread.sleep(2000);
				//Type in location
				driver.findElement(By.id("location")).sendKeys(""+sh.getCell(0, 26).getContents());
				Thread.sleep(2000);
				//click on create button
				driver.findElement(By.xpath("//*[@id='create']")).click();
				Thread.sleep(2000);
				//click on memberlist
				driver.findElement(By.xpath("//*[@id='memberListLink']/a")).click();
				Thread.sleep(2000); 
//===================================================================================================================================//				
				//Create on second partner
				//click on create partner
				WebDriverWait wait6 = new WebDriverWait(driver, 30); 
				wait6.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='createPrtnrLink']/a"))).click();
				//driver.findElement(By.xpath("//*[@id='createPrtnrLink']/a")).click();
				Thread.sleep(2000);
				//Type in company name
				driver.findElement(By.id("companyName")).sendKeys(""+sh.getCell(0, 28).getContents());
				Thread.sleep(2000);
				//Select the industry
				Select select8 = new Select(driver.findElement(By.xpath("//*[@id='industry']")));
				select8.selectByVisibleText("Fashion");
				Thread.sleep(2000);
				//Type in address
				driver.findElement(By.id("addressLine1")).sendKeys(""+sh.getCell(0, 29).getContents());
				Thread.sleep(2000);
				driver.findElement(By.id("addressLine2")).sendKeys(""+sh.getCell(0, 30).getContents());
				Thread.sleep(2000);
				//Select on country
				Select select9 = new Select(driver.findElement(By.xpath("//*[@id='country']")));
				select9.selectByVisibleText("India");
				//Select on city
				Select select10 = new Select(driver.findElement(By.xpath("//*[@id='city']")));
				select10.selectByVisibleText("Bengaluru");
				
				//Type in postal code
				driver.findElement(By.id("postalCode")).sendKeys(""+sh.getCell(0, 31).getContents());
				//Type in company description
				driver.findElement(By.id("companyDesc")).sendKeys(""+sh.getCell(0, 32).getContents());
				Thread.sleep(2000);
				// select exclude Partners Display
				Select select11 = new Select(driver.findElement(By.xpath("//*[@id='excludePartnersDisplay']")));
				select11.selectByVisibleText("Consultant Bench");
				Thread.sleep(2000);
				//select exclude Partners Publish
				Select select12 = new Select(driver.findElement(By.xpath("//*[@id='excludePartnersPublish']")));
				select12.selectByVisibleText("Enstrapp IT Solutions Pvt Ltd");
				Thread.sleep(2000);
				//select in multiple companyOnboardingDocs
				Select select13 = new Select(driver.findElement(By.xpath("//*[@id='companyOnboardingDocs']")));
				select13.selectByVisibleText("Photo Copy Passport");
				select13.selectByVisibleText("Pan/Ration Cards");
				select13.selectByVisibleText("Passport/Stamp size Photo");
				select13.selectByVisibleText("Last Employer Pay Slip");
				Thread.sleep(2000);
				//KEY CONTACT PERSON
		        //Type in contact person first name
				driver.findElement(By.id("kContactFirstName")).sendKeys(""+sh.getCell(0, 33).getContents());
				Thread.sleep(2000);
				//Type in contect person last name
				driver.findElement(By.id("keyContact.lastName")).sendKeys(""+sh.getCell(0, 34).getContents());
				Thread.sleep(2000);
		        //Type in location
				driver.findElement(By.id("keyContact.location")).sendKeys(""+sh.getCell(0, 35).getContents());
				Thread.sleep(2000);
				//Type in EMailID
				driver.findElement(By.id("kContactEmail")).sendKeys(""+sh.getCell(0, 36).getContents());
				Thread.sleep(2000);
				//Type in contact number
				driver.findElement(By.id("kContactPhone")).sendKeys(""+sh.getCell(0, 37).getContents());
				Thread.sleep(2000);
				//Type in google hangout id
				driver.findElement(By.id("keyContact.collaborationId")).sendKeys(""+sh.getCell(0, 38).getContents());
				//TAX INFORMATION
				//Type in service tax
				driver.findElement(By.id("servicesTax")).sendKeys(""+sh.getCell(0, 39).getContents());
				Thread.sleep(2000);
				//Type in VAT
				driver.findElement(By.id("valueAddedTax")).sendKeys(""+sh.getCell(0, 40).getContents());
				Thread.sleep(2000);
				//Type in PAN number
				driver.findElement(By.id("pan")).sendKeys(""+sh.getCell(0, 41).getContents());
				Thread.sleep(2000);
				//Type in TIN number
				driver.findElement(By.id("tin")).sendKeys(""+sh.getCell(0, 42).getContents());
				Thread.sleep(2000);
				//PAYMENT DETAILS
				//Type in BANKNAME
				driver.findElement(By.id("companyPaymentDetails.bankName")).sendKeys(""+sh.getCell(0, 43).getContents());
				Thread.sleep(2000);
				//Type in ACC No
				driver.findElement(By.id("companyPaymentDetails.accountNumber")).sendKeys(""+sh.getCell(0, 44).getContents());
				Thread.sleep(2000);
				//Type in ACC Holder name
				driver.findElement(By.id("companyPaymentDetails.accountHolderName")).sendKeys(""+sh.getCell(0, 45).getContents());
				Thread.sleep(2000);
				//Type in IFSC code
				driver.findElement(By.id("companyPaymentDetails.ifscCode")).sendKeys(""+sh.getCell(0, 46).getContents());
				Thread.sleep(2000);
				//Type in BRANCH
				driver.findElement(By.id("companyPaymentDetails.branch")).sendKeys(""+sh.getCell(0, 47).getContents());
				Thread.sleep(2000);
				//Click on create partner button
				driver.findElement(By.id("submitButton")).click();
				Thread.sleep(2000);
				//Click on partner list  
				driver.findElement(By.xpath("//*[@id='prtnrListLink']/a")).click();
				//Click on company enable checkbox
				List<WebElement> els1 = driver.findElements(By.xpath("//input[@type='checkbox']"));
		        for ( WebElement el : els1 ) 
		        {
		            if ( !el.isSelected() ) 
		            {
		                el.click();
			}

		}
		        
		        driver.navigate().back();
		        //click on create member
		        WebDriverWait wait2 = new WebDriverWait(driver, 30); 
				wait2.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='createMemberLink']/a"))).click();
		        //driver.findElement(By.xpath("//*[@id='createMemberLink']/a")).click();
				Thread.sleep(2000);
				//Type in username
				driver.findElement(By.id("username")).sendKeys(""+sh.getCell(0, 48).getContents());
				Thread.sleep(2000);
				//Type in password
				driver.findElement(By.id("password")).sendKeys(""+sh.getCell(0, 49).getContents());
				Thread.sleep(2000);
				//Select on company name
				Select select14=new Select(driver.findElement(By.id("company")));
				select14.selectByVisibleText("Infosys");
				//Type in first name
				driver.findElement(By.id("firstName")).sendKeys(""+sh.getCell(0, 50).getContents());
				Thread.sleep(2000);
				//Type in lastname
				driver.findElement(By.id("lastName")).sendKeys(""+sh.getCell(0, 51).getContents());
				Thread.sleep(2000);
				//Select on role
				Select select15=new Select(driver.findElement(By.id("role.id")));
				select15.selectByVisibleText("SUPER_USER");
				Thread.sleep(2000);
				//Type in contact number
				driver.findElement(By.id("contactNo")).sendKeys(""+sh.getCell(0, 52).getContents());
				Thread.sleep(2000);
				//Type in location
				driver.findElement(By.id("location")).sendKeys(""+sh.getCell(0, 53).getContents());
				Thread.sleep(2000);
				//click on create button
				driver.findElement(By.xpath("//*[@id='create']")).click();
				Thread.sleep(2000);
				//click on memberlist
				driver.findElement(By.xpath("//*[@id='memberListLink']/a")).click();
				Thread.sleep(2000);           
//==============================================================================================================================//				
				//Create on third(3) partner
				//click on create partner
				WebDriverWait wait3 = new WebDriverWait(driver, 30); 
				wait3.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='createPrtnrLink']/a"))).click();
				//driver.findElement(By.xpath("//*[@id='createPrtnrLink']/a")).click();
				Thread.sleep(2000);
				//Type in company name
				driver.findElement(By.id("companyName")).sendKeys(""+sh.getCell(0, 55).getContents());
				Thread.sleep(2000);
				//Select the industry
				Select select16 = new Select(driver.findElement(By.xpath("//*[@id='industry']")));
				select16.selectByVisibleText("Fashion");
				Thread.sleep(2000);
				//Type in address
				driver.findElement(By.id("addressLine1")).sendKeys(""+sh.getCell(0, 56).getContents());
				Thread.sleep(2000);
				driver.findElement(By.id("addressLine2")).sendKeys(""+sh.getCell(0, 57).getContents());
				Thread.sleep(2000);
				//Select on country
				Select select17 = new Select(driver.findElement(By.xpath("//*[@id='country']")));
				select17.selectByVisibleText("India");
				//Select on city
				Select select18 = new Select(driver.findElement(By.xpath("//*[@id='city']")));
				select18.selectByVisibleText("Bengaluru");
				Thread.sleep(2000);
				//Type in postal code
				driver.findElement(By.id("postalCode")).sendKeys(""+sh.getCell(0, 58).getContents());
				//Type in company description
				driver.findElement(By.id("companyDesc")).sendKeys(""+sh.getCell(0, 59).getContents());
				Thread.sleep(2000);
				// select exclude Partners Display
				Select select19 = new Select(driver.findElement(By.xpath("//*[@id='excludePartnersDisplay']")));
				select19.selectByVisibleText("Consultant Bench");
				
				//select exclude Partners Publish
				Select select20 = new Select(driver.findElement(By.xpath("//*[@id='excludePartnersPublish']")));
				select20.selectByVisibleText("Enstrapp IT Solutions Pvt Ltd");
				Thread.sleep(2000);
				//select in multiple companyOnboardingDocs
				Select select21 = new Select(driver.findElement(By.xpath("//*[@id='companyOnboardingDocs']")));
				select21.selectByVisibleText("Photo Copy Passport");
				select21.selectByVisibleText("Pan/Ration Cards");
				select21.selectByVisibleText("Passport/Stamp size Photo");
				select21.selectByVisibleText("Last Employer Pay Slip");
				Thread.sleep(2000);
				//KEY CONTACT PERSON
		        //Type in contact person first name
				driver.findElement(By.id("kContactFirstName")).sendKeys(""+sh.getCell(0, 60).getContents());
				Thread.sleep(2000);
				//Type in contect person last name
				driver.findElement(By.id("keyContact.lastName")).sendKeys(""+sh.getCell(0, 61).getContents());
				Thread.sleep(2000);
		        //Type in location
				driver.findElement(By.id("keyContact.location")).sendKeys(""+sh.getCell(0, 62).getContents());
				Thread.sleep(2000);
				//Type in EMailID
				driver.findElement(By.id("kContactEmail")).sendKeys(""+sh.getCell(0, 63).getContents());
				Thread.sleep(2000);
				//Type in contact number
				driver.findElement(By.id("kContactPhone")).sendKeys(""+sh.getCell(0, 64).getContents());
				Thread.sleep(2000);
				//Type in google hangout id
				driver.findElement(By.id("keyContact.collaborationId")).sendKeys(""+sh.getCell(0, 65).getContents());
				//TAX INFORMATION
				//Type in service tax
				driver.findElement(By.id("servicesTax")).sendKeys(""+sh.getCell(0, 66).getContents());
				Thread.sleep(2000);
				//Type in VAT
				driver.findElement(By.id("valueAddedTax")).sendKeys(""+sh.getCell(0, 67).getContents());
				Thread.sleep(2000);
				//Type in PAN number
				driver.findElement(By.id("pan")).sendKeys(""+sh.getCell(0, 68).getContents());
				Thread.sleep(2000);
				//Type in TIN number
				driver.findElement(By.id("tin")).sendKeys(""+sh.getCell(0, 69).getContents());
				Thread.sleep(2000);
				//PAYMENT DETAILS
				//Type in BANKNAME
				driver.findElement(By.id("companyPaymentDetails.bankName")).sendKeys(""+sh.getCell(0, 70).getContents());
				Thread.sleep(2000);
				//Type in ACC No
				driver.findElement(By.id("companyPaymentDetails.accountNumber")).sendKeys(""+sh.getCell(0, 71).getContents());
				Thread.sleep(2000);
				//Type in ACC Holder name
				driver.findElement(By.id("companyPaymentDetails.accountHolderName")).sendKeys(""+sh.getCell(0, 72).getContents());
				Thread.sleep(2000);
				//Type in IFSC code
				driver.findElement(By.id("companyPaymentDetails.ifscCode")).sendKeys(""+sh.getCell(0, 73).getContents());
				Thread.sleep(2000);
				//Type in BRANCH
				driver.findElement(By.id("companyPaymentDetails.branch")).sendKeys(""+sh.getCell(0, 74).getContents());
				Thread.sleep(2000);
				//Click on create partner button
				driver.findElement(By.id("submitButton")).click();
				Thread.sleep(2000);
				//Click on partner list  
				driver.findElement(By.xpath("//*[@id='prtnrListLink']/a")).click();
				//Click on company enable checkbox
				List<WebElement> els2 = driver.findElements(By.xpath("//input[@type='checkbox']"));
		        for ( WebElement el : els2 ) 
		        {
		            if ( !el.isSelected() )
		            {
		                el.click();
			}

		}
		       
		        driver.navigate().back(); 
		        //click on create member
		        WebDriverWait wait1 = new WebDriverWait(driver, 30); 
				wait1.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='createMemberLink']/a"))).click();
		        //driver.findElement(By.xpath("//*[@id='createMemberLink']/a")).click();
				Thread.sleep(2000);
				//Type in username
				driver.findElement(By.id("username")).sendKeys(""+sh.getCell(0, 75).getContents());
				Thread.sleep(2000);
				//Type in password
				driver.findElement(By.id("password")).sendKeys(""+sh.getCell(0, 76).getContents());
				Thread.sleep(2000);
				//Select on company name
				Select select22=new Select(driver.findElement(By.id("company")));
				select22.selectByVisibleText("Hewlett Packard Enterprise");
				//Type in first name
				driver.findElement(By.id("firstName")).sendKeys(""+sh.getCell(0, 77).getContents());
				Thread.sleep(2000);
				//Type in lastname
				driver.findElement(By.id("lastName")).sendKeys(""+sh.getCell(0, 78).getContents());
				Thread.sleep(2000);
				//Select on role
				Select select23=new Select(driver.findElement(By.id("role.id")));
				select23.selectByVisibleText("SUPER_USER");
				Thread.sleep(2000);
				//Type in contact number
				driver.findElement(By.id("contactNo")).sendKeys(""+sh.getCell(0, 79).getContents());
				Thread.sleep(2000);
				//Type in location
				driver.findElement(By.id("location")).sendKeys(""+sh.getCell(0, 80).getContents());
				Thread.sleep(2000);
				//click on create button
				driver.findElement(By.xpath("//*[@id='create']")).click();
				Thread.sleep(2000);
				//click on memberlist
				driver.findElement(By.xpath("//*[@id='memberListLink']/a")).click();
				Thread.sleep(2000);
	//==========================================================================================================//			
			

		

		
		

	}

}
