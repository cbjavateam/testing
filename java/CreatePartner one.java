package readexcel;

import java.io.FileInputStream;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class Createpartner {

	public static void main(String[] args) throws BiffException, IOException, InterruptedException 
	{
		WebDriver driver = new FirefoxDriver();
		String FilePath = "C:\\ConsultantsBench\\partnerbook.xls";
		FileInputStream fs = new FileInputStream(FilePath);
		Workbook wb = Workbook.getWorkbook(fs);
		Sheet sh = wb.getSheet("partner");
		int totalNoOfRows = sh.getRows();
		int totalNoOfCols = sh.getColumns();
		for (int row = 0; row < totalNoOfRows; row++) 
		{
				for (int col = 0; col < totalNoOfCols; col++) 
				{
					System.out.print(sh.getCell(col, row).getContents() + "\t");
				}
				System.out.println();
		}
		//navigate url
		driver.get("http://stage.consultantsbench.com/member/decideUrl");
		//Type in Emailid and password
		driver.findElement(By.id("username")).sendKeys(""+sh.getCell(0, 0).getContents());
		driver.findElement(By.id("password")).sendKeys(""+sh.getCell(1, 0).getContents());
		Thread.sleep(2000);
		//click on sing in button
		driver.findElement(By.id("signInBtn")).click();
		//click on create partner
		driver.findElement(By.xpath("//*[@id='createPrtnrLink']/a")).click();
		Thread.sleep(2000);
		//Type in company name
		driver.findElement(By.id("companyName")).sendKeys(""+sh.getCell(0, 1).getContents());
		Thread.sleep(2000);
		//Select the industry
		Select select = new Select(driver.findElement(By.xpath("//*[@id='industry']")));
		select.selectByVisibleText("Fashion");
		Thread.sleep(2000);
		//Type in address
		driver.findElement(By.id("addressLine1")).sendKeys(""+sh.getCell(0, 2).getContents());
		Thread.sleep(2000);
		driver.findElement(By.id("addressLine2")).sendKeys(""+sh.getCell(0, 3).getContents());
		Thread.sleep(2000);
		//Select on country
		Select select1 = new Select(driver.findElement(By.xpath("//*[@id='country']")));
		select1.selectByVisibleText("United States");
		//Select on city
		Select select2 = new Select(driver.findElement(By.xpath("//*[@id='city']")));
		select2.selectByVisibleText("Bengaluru");
		Thread.sleep(2000);
		//Type in postal code
		driver.findElement(By.id("postalCode")).sendKeys(""+sh.getCell(0, 4).getContents());
		//Type in company description
		driver.findElement(By.id("companyDesc")).sendKeys(""+sh.getCell(0, 5).getContents());
		Thread.sleep(2000);
		// select exclude Partners Display
		Select select3 = new Select(driver.findElement(By.xpath("//*[@id='excludePartnersDisplay']")));
		select3.selectByVisibleText("Consultant Bench");
		Thread.sleep(2000);
		//select exclude Partners Publish
		Select select4 = new Select(driver.findElement(By.xpath("//*[@id='excludePartnersPublish']")));
		select4.selectByVisibleText("Enstrapp IT Solutions Pvt Ltd");
		Thread.sleep(2000);
		//select in multiple companyOnboardingDocs
		Select select5 = new Select(driver.findElement(By.xpath("//*[@id='companyOnboardingDocs']")));
		select5.selectByVisibleText("Photo Copy Passport");
		select5.selectByVisibleText("Pan/Ration Cards");
		select5.selectByVisibleText("Passport/Stamp size Photo");
		select5.selectByVisibleText("Last Employer Pay Slip");
		Thread.sleep(2000);
		//KEY CONTACT PERSON
        //Type in contact person first name
		driver.findElement(By.id("kContactFirstName")).sendKeys(""+sh.getCell(0, 6).getContents());
		Thread.sleep(2000);
		//Type in contect person last name
		driver.findElement(By.id("keyContact.lastName")).sendKeys(""+sh.getCell(0, 7).getContents());
		Thread.sleep(2000);
        //Type in location
		driver.findElement(By.id("keyContact.location")).sendKeys(""+sh.getCell(0, 8).getContents());
		Thread.sleep(2000);
		//Type in EMailID
		driver.findElement(By.id("kContactEmail")).sendKeys(""+sh.getCell(0, 9).getContents());
		Thread.sleep(2000);
		//Type in contact number
		driver.findElement(By.id("kContactPhone")).sendKeys(""+sh.getCell(0, 10).getContents());
		Thread.sleep(2000);
		//Type in google hangout id
		driver.findElement(By.id("keyContact.collaborationId")).sendKeys(""+sh.getCell(0, 11).getContents());
		//TAX INFORMATION
		//Type in service tax
		driver.findElement(By.id("servicesTax")).sendKeys(""+sh.getCell(0, 12).getContents());
		Thread.sleep(2000);
		//Type in VAT
		driver.findElement(By.id("valueAddedTax")).sendKeys(""+sh.getCell(0, 13).getContents());
		Thread.sleep(2000);
		//Type in PAN number
		driver.findElement(By.id("pan")).sendKeys(""+sh.getCell(0, 14).getContents());
		Thread.sleep(2000);
		//Type in TIN number
		driver.findElement(By.id("tin")).sendKeys(""+sh.getCell(0, 15).getContents());
		Thread.sleep(2000);
		//PAYMENT DETAILS
		//Type in BANKNAME
		driver.findElement(By.id("companyPaymentDetails.bankName")).sendKeys(""+sh.getCell(0, 16).getContents());
		Thread.sleep(2000);
		//Type in ACC No
		driver.findElement(By.id("companyPaymentDetails.accountNumber")).sendKeys(""+sh.getCell(0, 17).getContents());
		Thread.sleep(2000);
		//Type in ACC Holder name
		driver.findElement(By.id("companyPaymentDetails.accountHolderName")).sendKeys(""+sh.getCell(0, 18).getContents());
		Thread.sleep(2000);
		//Type in IFSC code
		driver.findElement(By.id("companyPaymentDetails.ifscCode")).sendKeys(""+sh.getCell(0, 19).getContents());
		Thread.sleep(2000);
		//Type in BRANCH
		driver.findElement(By.id("companyPaymentDetails.branch")).sendKeys(""+sh.getCell(0, 20).getContents());
		Thread.sleep(2000);
		//Click on create partner button
		//driver.findElement(By.xpath("//*[@id='submitButton']")).click();
		driver.findElement(By.id("submitButton")).click();

	}

}
