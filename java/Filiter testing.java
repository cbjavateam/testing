package selenium;



import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FunctionTesting {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver=new FirefoxDriver();
		//enter the url
		driver.get("http://stage.consultantsbench.com/member/decideUrl");
		//type the username
		driver.findElement(By.xpath(".//*[@id='username']")).sendKeys("rishi.roy@infogain.com");
        //type the password
		driver.findElement(By.xpath(".//*[@id='password']")).sendKeys("cb4infogain");
		//click on signin button
		driver.findElement(By.xpath(".//*[@id='loginForm']/button")).click();
		//click on search button
		driver.findElement(By.xpath("html/body/header/div/div/div[2]/form/div/div[3]/button")).click();
		Thread.sleep(5000);
        //click on industry dropdown and open one text box
		driver.findElement(By.xpath(".//*[@id='filtersForm']/ul/li[1]/a")).click();
		//search in select one industry
		driver.findElement(By.xpath(".//*[@id='dropdownAB1']/div[1]/ul/li/div")).click();
		//click on hightech&electronics
		driver.findElement(By.xpath(".//*[@id='ul_dropdownAB1']/ul/li[42]/a")).click();
		//click on refresh botton
		driver.findElement(By.xpath(".//*[@id='filtImg']")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("html/body/header/div/div/div[2]/form/div/div[3]/button")).click();
		Thread.sleep(5000);
		//go filter in secondary skills
		driver.findElement(By.xpath(".//*[@id='filtersForm']/ul/li[2]/a")).click();
		//go to dropdown
		driver.findElement(By.xpath(".//*[@id='dropdownAB2']/div[1]/ul/li/div")).click();
		//click in c#
		driver.findElement(By.xpath(".//*[@id='ul_dropdownAB2']/ul/li[96]/a")).click();
		//And refresh one time
		driver.findElement(By.xpath(".//*[@id='filtImg']")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("html/body/header/div/div/div[2]/form/div/div[3]/button")).click();
		Thread.sleep(5000);
		//click on years of experience
		driver.findElement(By.xpath(".//*[@id='filtersForm']/ul/li[3]/a")).click();
		//and select range
		driver.findElement(By.xpath(".//*[@id='slider-range']/div")).click();
		driver.findElement(By.xpath(".//*[@id='filtImg']")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("html/body/header/div/div/div[2]/form/div/div[3]/button")).click();
		Thread.sleep(5000);
		//click on experience type
	    driver.findElement(By.xpath("//*[@id='filtersForm']/ul/li[4]/a")).click();
	    //inside the support checkbox
	    driver.findElement(By.xpath("//div[@id='panel5a']/label[1]/span")).click();
	    driver.findElement(By.xpath(".//*[@id='filtImg']")).click();
	    Thread.sleep(5000);
		driver.findElement(By.xpath("html/body/header/div/div/div[2]/form/div/div[3]/button")).click();
	    Thread.sleep(5000);
	    //click on location
	    driver.findElement(By.xpath("//*[@id='filtersForm']/ul/li[5]/a")).click();
	    //click on inside location indropdown box
	    driver.findElement(By.xpath("//*[@id='dropdownAB3']/div[1]/ul/li/div")).click();
	    //And select the place
	    driver.findElement(By.xpath("//*[@id='ul_dropdownAB3']/ul/li[1]/a")).click();
	    driver.findElement(By.xpath(".//*[@id='filtImg']")).click();
	    Thread.sleep(5000);
		driver.findElement(By.xpath("html/body/header/div/div/div[2]/form/div/div[3]/button")).click();
	    Thread.sleep(5000);
	    //click on Availability 
	    driver.findElement(By.xpath(".//*[@id='filtersForm']/ul/li[6]/a/span")).click();
	    //And select on checkbox
	    driver.findElement(By.xpath("//*[@id='panel7a']/label[1]")).click();
	    driver.findElement(By.xpath(".//*[@id='filtImg']")).click();
	    Thread.sleep(5000);
		driver.findElement(By.xpath("html/body/header/div/div/div[2]/form/div/div[3]/button")).click();
	    Thread.sleep(5000);
	    //click on rate of month
	    driver.findElement(By.xpath("//*[@id='filtersForm']/ul/li[7]/a")).click();
	    //click on INR salary
	    driver.findElement(By.xpath("//*[@id='rate-range']/div")).click();
	    driver.findElement(By.xpath(".//*[@id='filtImg']")).click();
	    Thread.sleep(5000);
		driver.findElement(By.xpath("html/body/header/div/div/div[2]/form/div/div[3]/button")).click();
	    Thread.sleep(5000);
	    //click on valitation
	    driver.findElement(By.xpath("//*[@id='filtersForm']/ul/li[8]/a")).click();
	    //click on verification
	    driver.findElement(By.xpath("//*[@id='panel8a']/label[1]/span")).click();
	    driver.findElement(By.xpath(".//*[@id='filtImg']")).click();
	    Thread.sleep(5000);
		driver.findElement(By.xpath("html/body/header/div/div/div[2]/form/div/div[3]/button")).click();
	    Thread.sleep(5000);
	    //click on Diversity
	    driver.findElement(By.xpath("//*[@id='filtersForm']/ul/li[9]/a")).click();
		// click on anyone in the diversity
	    driver.findElement(By.xpath("//*[@id='panel9a']/label[2]/span")).click();
	    driver.findElement(By.xpath(".//*[@id='filtImg']")).click();
	    driver.findElement(By.xpath("html/body/header/div/div/div[2]/form/div/div[3]/button")).click();
	
	}
}	