package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FunctionTestingMemberlist {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver=new FirefoxDriver();
		driver.get("http://stage.consultantsbench.com/member/decideUrl");
		//type the username
		driver.findElement(By.xpath("//*[@id='username']")).sendKeys("admin@cbench.com");
		 //type the password
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys("admin");
		//click on signin button
		driver.findElement(By.xpath("//*[@id='loginForm']/button")).click();
		//click on partner list
		driver.findElement(By.xpath("//*[@id='prtnrListLink']/a")).click();
		//click on partnerlist inside Consultant Bench
		driver.findElement(By.xpath("//*[@id='list-company']/table/tbody/tr[1]/td[1]/a")).click();
		Thread.sleep(2000);
		//click on memberlist
		driver.findElement(By.xpath("//*[@id='memberListLink']/a")).click();
		//click on memberlist inside the sreesha name
		driver.findElement(By.xpath(".//*[@id='list-member']/table/tbody/tr[3]/td[1]/a")).click();
		
	}

}
